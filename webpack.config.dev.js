const merge = require("webpack-merge")
const common = require("./webpack.config.common")

module.exports = merge(common, {
    mode: "development",
    devtool: "eval-source-map",
    devServer: {
        contentBase: "./dist",
        port: 8080
    },
    module: {
        rules: [{
            test: /\.less$/,
            use: [
                "style-loader",
                "css-loader",
                "less-loader"
            ]
        }]
    }
})
