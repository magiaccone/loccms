export default [{
    id: "f1",
    type: "feature",
    title: "Feature 1",
    children: [{
        id: "f1c1",
        type: "context",
        title: "Context 1.1"
    }, {
        id: "f1c2",
        type: "context",
        title: "Context 1.2",
        children: [
            {
                id: "f1c2e1",
                type: "event",
                title: "Event 1.2.1",
                children: [
                    {id: "f1c2e1s1", type: "sentence", title: "Sentence 1.2.1.1"},
                    {id: "f1c2e1s2", type: "sentence", title: "Sentence 1.2.1.2"}
                ]
            },
            {
                id: "f1c2s2",
                type: "event",
                title: "Event 1.2.2"
            }
        ]
    }]
}, {
    id: "f2",
    type: "feature",
    title: "Feature 2"
}]
