export const SET_SELECTED_NODE_ID = "SET_SELECTED_NODE_ID"
export const DELETE_NODE = "DELETE_NODE"
export const ADD_NODE = "ADD_NODE"

export const setSelectedNodeId = nodeId => ({type: SET_SELECTED_NODE_ID, nodeId})

export const deleteNode = nodeId => ({type: DELETE_NODE, nodeId})

export const addNode = (nodeId, nodeName) => ({type: ADD_NODE, nodeId, nodeName})
