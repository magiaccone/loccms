import DEFAULT_STATE from "./DefaultState"
import {DELETE_NODE, SET_SELECTED_NODE_ID, ADD_NODE} from './Actions'

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case SET_SELECTED_NODE_ID:
            return {...state, selectedNodeId: action.nodeId}

        case ADD_NODE:
            const newNode = {
                id: action.nodeName,
                type: "sentence",
                title: action.nodeName,
                children: null
            }

            const makeTree = (data) =>
                _.reduce(data, (tree, element) =>
                        _.concat(tree, {
                            id: element.id,
                            type: element.type,
                            title: element.title,
                            children: _.concat(
                                element.id === action.nodeId ? [newNode] : [],
                                element.children ? makeTree(element.children) : []
                            )
                        })
                    , [])

            return {...state, speechTree: makeTree(state.speechTree)}

        case DELETE_NODE:
            const filterTree = (data) =>
                _.reduce(data, (tree, element) => {
                    return element.id !== action.nodeId ? _.concat(tree, {
                        id: element.id,
                        type: element.type,
                        title: element.title,
                        children: element.children ? filterTree(element.children) : []
                    }) : tree
                }, [])

            return {...state, speechTree: filterTree(state.speechTree)}
        default:
            return state
    }
}
