import React from 'react'
import ReactDom from 'react-dom'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import Reducers from  "./state/Reducers"
import App from "./ui/App"

const store = createStore(Reducers)

ReactDom.render((
    <Provider store={store}>
        <App />
    </Provider>
), document.getElementById('app'))
