import {connect} from "react-redux"
import SentenceOverview from "./SentenceOverview"

const mapStateToProps = (state) => {
    return {
        title: state.selectedNodeId ? `Overview of "${state.selectedNodeId}"` : "No sentence selected"
    }
}

export default connect(mapStateToProps)(SentenceOverview)
