import React from "react"

import "./sentenceOverview.less"

class SentenceOverview extends React.Component {
    constructor(props) {
        super(props)

    }

    render(){
        const {title} = this.props

        return (
            <div className="sentenceOverview">
                <span className="sentenceOverview__title">{title}</span>
            </div>
        )
    }
}

export default SentenceOverview
