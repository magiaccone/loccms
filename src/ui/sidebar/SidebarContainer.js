import {connect} from "react-redux"
import Sidebar from "./Sidebar"
import {deleteNode, setSelectedNodeId, addNode} from "../../state/Actions"

const mapDispatchToProps = (dispatch) => ({
    onNodeClick: id => dispatch(setSelectedNodeId(id)),
    onNodeDelete: id => dispatch(deleteNode(id)),
    onNodeAdd: (id, nodeName) => dispatch(addNode(id, nodeName))
})

const mapStateToProps = (state) => {
    return {
        tree: state.speechTree,
        selectedNodeId: state.selectedNodeId
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
