import React from "react"
import Tree from "./components/tree/Tree"

import "./sidebar.less"

class Sidebar extends React.Component {
    render() {
        const {tree, selectedNodeId, onNodeClick, onNodeDelete, onNodeAdd} = this.props

        return (
            <div className="sidebar">
                <div className="sidebar__header">
                    <span className="sidebar__title">List Of Sentences</span>
                </div>

                <div className="sidebar__speechTree">
                    <Tree onNodeClick={onNodeClick}
                          onNodeDelete={onNodeDelete}
                          onNodeAdd={onNodeAdd}
                          selectedNodeId={selectedNodeId}
                          data={tree}/>
                </div>
            </div>
        )
    }
}

export default Sidebar
