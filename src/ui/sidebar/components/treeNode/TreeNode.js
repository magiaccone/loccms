import React from "react"
import _ from "lodash"

import "./treeNode.less"

const DEPTH_PADDING = 30

export default class TreeNode extends React.Component {
    constructor(props) {
        super(props)
    }

    onClick = () => {
        const {data, onClick} = this.props
        onClick && onClick(data.id)
    }

    onExpanderClick = e => {
        const {data, onExpanderClick} = this.props
        e.stopPropagation()
        onExpanderClick && onExpanderClick(data.id)
    }

    onButtonClick = e => {
        const {data, onDelete, onAdd} = this.props

        e.stopPropagation()

        if (data.type === "event") {
            onAdd && onAdd(data.id, `sentence ${_.uniqueId()}`)
        } else {
            onDelete && onDelete(data.id)
        }
    }

    render() {
        const {data, depthLevel, expandedNodesIds, onClick, onDelete, onAdd, onExpanderClick, selectedNodeId} = this.props
        const style = {paddingLeft: depthLevel * DEPTH_PADDING}
        const expanded = !expandedNodesIds || _.includes(expandedNodesIds, data.id)
        const selected = selectedNodeId === data.id
        const expandable = data.type !== "sentence" && data.children && data.children.length > 0

        return (
            <li className={`treeNode${selected ? " treeNode--selected" : ""}`}>
                <div className={`treeNode__title${expandable ? " treeNode__title--expandable" : ""}`}
                     style={style}
                     onClick={this.onClick}>
                    <div className={`treeNode__titleExpander${expanded ? " treeNode__titleExpander--expanded" : ""}`}
                         onClick={expandable ? this.onExpanderClick : null}>
                        ▶
                    </div>

                    <span className="treeNode__titleLabel" title={data.title}>{data.title}</span>

                    {_.includes(["sentence", "event"], data.type) &&
                    <div className={`treeNode__button treeNode__button--${data.type === "event" ? "add" : "delete"}`}
                         onClick={this.onButtonClick}>
                        <span>+</span>
                    </div>}
                </div>

                {data.children && expanded &&
                <ul className="treeNode__children">
                    {_.map(data.children, nodeData => (
                        <TreeNode
                            key={nodeData.id}
                            data={nodeData}
                            depthLevel={depthLevel + 1}
                            onClick={onClick}
                            onDelete={onDelete}
                            onAdd={onAdd}
                            onExpanderClick={onExpanderClick}
                            selectedNodeId={selectedNodeId}
                            expandedNodesIds={expandedNodesIds}
                        />
                    ))}
                </ul>}
            </li>
        )
    }
}
