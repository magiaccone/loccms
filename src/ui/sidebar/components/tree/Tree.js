import React from "react"
import _ from "lodash"
import TreeNode from "../treeNode/TreeNode"

import "./tree.less"

export default class Tree extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            expandedNodesIds: []
        }
    }

    onNodeExpanderClick = (nodeId) => {
        const {expandedNodesIds} = this.state
        this.setState({expandedNodesIds: _.xor(expandedNodesIds, [nodeId])})
    }

    render() {
        const {data, onNodeClick, selectedNodeId, onNodeDelete, onNodeAdd} = this.props
        const {expandedNodesIds} = this.state

        return (
            <ul className="tree">
                {_.map(data, node => (
                    <TreeNode
                        key={node.id}
                        data={node}
                        depthLevel={0}
                        onClick={onNodeClick}
                        onExpanderClick={this.onNodeExpanderClick}
                        expandedNodesIds={expandedNodesIds}
                        selectedNodeId={selectedNodeId}
                        onDelete={onNodeDelete}
                        onAdd={onNodeAdd}
                    />
                ))}
            </ul>
        )
    }
}
