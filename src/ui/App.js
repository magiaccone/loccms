import React from "react"
import SidebarContainer from "./sidebar/SidebarContainer"
import SentenceOverview from "./sentenceOverview/SentenceOverviewContainer"

import "./app.less"

export default class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <React.Fragment>
                <SidebarContainer/>
                <SentenceOverview/>
            </React.Fragment>
        )
    }
}
