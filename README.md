# LocCMS

This is my answer to the EA coding test. The project is build with webpack, and is coded with React/Redux.

I tried to do it in a maximum of three hours. But it takes me a little more time to add the "add/remove sentences" feature.

## Run the project

After cloning this repo, enter the commands below to run the project :

`npm install`

`npm start`

Then in a browser, go to **localhost:8080**

## What I've done

- The initial data is hard-coded and saved into the Redux state. I supposed this data was got from a back end server.
- The tree is displayed in a sidebar, on the left. The right part is reserved to the sentences details and, I guess, the associated translations and data.
- The tree nodes are expandable so that the navigation is nicer. We could add a checkbox to expand all the nodes if we want the complete list.
- You can add a sentence by clicking the "+" button of an event. In a first time, the sentence is made with a generated name. With more time we could use a field or a small pop up to set the name and some informations.
- You can remove a sentence by clicking the "-" button of a sentence. We could add add a confirmation dialog to prevent missclicks.
- The application has been tested in Chrome, Firefox, IE and Edge but I didn't work on the responsive aspect.

## Tests

I didn't have time to add tests but I think it's important to do tests as soon as possible. Jest is very easy to setup, it would be a shame to not add test, the project is expected to grow.